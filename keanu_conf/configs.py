from typing import Dict, List, Type

from keanu_conf import AbstractConfiguration
from keanu_conf.keanu import KeanuConfiguration


def get_available_configurations_types() -> Dict[str, Type[AbstractConfiguration]]:
    return {"keanu": KeanuConfiguration}


def get_configuration_type_names() -> List[str]:
    return list(get_available_configurations_types().keys())
