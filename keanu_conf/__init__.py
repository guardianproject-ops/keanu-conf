import pathlib
from abc import abstractmethod
from dataclasses import dataclass
from typing import Optional, Type

from pydantic import BaseModel

from keanu_conf import sops


@dataclass
class Context:
    ops_root: pathlib.Path
    debug: bool


class AbstractConfiguration:
    __abstract__ = True

    short_name: str = "base"
    description: str = "Abstract configuration"
    id: str

    root_config: Optional[BaseModel]

    @abstractmethod
    def __init__(self, ctx: Context, id=None):
        self.ctx = ctx
        self.root_config = None
        self.id = id

    @property
    @abstractmethod
    def root_config_type(self) -> Type[BaseModel]:
        """
        The root level configuration type for the configuration
        """
        raise NotImplementedError()

    @abstractmethod
    def create_interactive(self) -> BaseModel:
        """
        Creates a configuration from scratch using an interactive console
        """
        raise NotImplementedError()

    @abstractmethod
    def create(self, root_config: BaseModel):
        """
        TODO: Create a configuration from a seed file.
        """
        raise NotImplementedError()

    @property
    def deployment_dir(self) -> pathlib.Path:
        """
        The full path to the deployment directory for this configuration
        """
        assert self.id
        deployment_folder = f"{self.short_name}-{self.id}"
        return (
            pathlib.Path(self.ctx.ops_root).joinpath("live").joinpath(deployment_folder)
        )

    def ensure_dirs(self) -> None:
        """
        Ensures that the necessary directories exist.

        Creates the deployment and config dir automatically, but does not create the ops root directory.
        """
        if not self.ctx.ops_root.exists():
            raise OSError(
                f"The root operations dir f{self.ctx.ops_root} does not exist. I will not create it for you."
            )
        if not self.deployment_dir.exists():
            self.deployment_dir.mkdir(parents=True)
        if not self.config_dir.exists():
            self.config_dir.mkdir(parents=True)

    @property
    def config_dir(self) -> pathlib.Path:
        """
        The full path to the configuration directory
        """
        return self.deployment_dir.joinpath("config")

    @property
    def primary_config_file(self) -> pathlib.Path:
        """
        The full path to the configuration file
        """
        return self.config_dir.joinpath("project.sops.json")

    def persist(self) -> None:
        """
        Saves the configuration to the config_dir
        """
        assert self.root_config
        payload = self.root_config.json()
        self.ensure_dirs()
        sops.encrypt_to(
            payload,
            sops.SopsFileType.json,
            sops.SopsFileType.json,
            self.primary_config_file,
            self.ctx.ops_root,
        )

    def load(self) -> None:
        """
        Load the configuration
        """
        if self.primary_config_file.exists():
            ciphertext = self.primary_config_file.read_text()
            plaintext = sops.decrypt(
                ciphertext,
                sops.SopsFileType.json,
                sops.SopsFileType.json,
                self.ctx.ops_root,
            )
            self.root_config = self.root_config_type.parse_raw(plaintext)
        else:
            print(
                f"Error: Cannot load configuration at {self.primary_config_file}. File does not exist."
            )

    def initialize_remote_state(self) -> None:
        """
        Initialize the remote terraform state resources
        """
        raise NotImplementedError()
