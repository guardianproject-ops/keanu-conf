# mypy: disallow_untyped_calls=False
import enum
import json
import pkgutil
from typing import Any


def _load():
    data = pkgutil.get_data(__name__, "data/language-codes.json")
    if not data:
        raise OSError("Could not load language code data")
    return json.loads(data)


def _get_langcodes(data: Any):
    r = {}
    for lang in data:
        r[lang["alpha2"]] = lang["alpha2"]
    return r


def _names_by_code(data: Any):
    r = {}
    for lang in data:
        r[lang["alpha2"]] = lang["English"]
    return r


def _build_enum():
    data = _load()
    names_by_code = _names_by_code(data)
    LanguageCode = enum.Enum("LanguageCode", _get_langcodes(data))

    for code in LanguageCode:
        code.english_name = names_by_code[code.value]
    return LanguageCode


LanguageCode = _build_enum()

default_language = LanguageCode.en
preferred_languages = [LanguageCode.en, LanguageCode.es]
