import re
import string

from signedjson import key as signedjson_key


def validate_domain(v: str) -> str:
    if v.count(".") >= 1 and v[0] != ".":
        return v
    raise ValueError(f"invalid domain: {v}")


def validate_identifier(v):
    allowed = set(string.ascii_lowercase + string.digits + "-")
    if set(v) <= allowed and set(v[0]) <= set(string.ascii_lowercase):
        return v
    raise ValueError(
        "Can only contain lowercase characters, digits and a hyphen. Must start with a lower case character"
    )


def validate_signing_key(v: str) -> str:
    try:
        decoded = signedjson_key.read_signing_keys([v])
        signedjson_key.get_verify_key(decoded[0])
        return v
    except ValueError:
        raise ValueError("signing key is invalid")


pattern_any_cidr = r"^([0-9]{1,3}\.){3}[0-9]{1,3}(\/([0-9]|[1-2][0-9]|3[0-2]))?$"
pattern_16_cidr = r"^([0-9]{1,3}\.){3}[0-9]{1,3}(\/16)$"

pattern_cidr = re.compile(
    r"""^                 # Start string
(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)\.            # A in A.B.C.D
(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)\.            # B in A.B.C.D
(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)\.            # C in A.B.C.D
(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)\/16          # D in A.B.C.D and /16
$                                              # End string""",
    re.X,
)


def validate_16_cidr(v: str) -> str:
    if re.match(pattern_cidr, v) is None:
        raise ValueError("invalid cidr. must be a /16 like 192.168.0.1/16")
    return v


def validate_int_multiple_of_10(v: int) -> int:
    if v % 10 != 0:
        raise ValueError("must be a multiple of 10")
    return v


def validate_url(v: str) -> str:
    import pydantic

    class Model(pydantic.BaseModel):
        v: pydantic.HttpUrl

    Model(v=v)
    return v
