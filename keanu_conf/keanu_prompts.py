# mypy: disallow_untyped_calls=False

from keanu_conf import interactive, models, util, validators
from keanu_conf.models_aws import Region
from keanu_conf.models_language import LanguageCode


def preset_questions_menu():
    region = interactive.prompt_region()
    az1 = region.zones[0]
    az2 = region.zones[1]
    replica_region = Region(interactive.replica_region_auto(region))
    interactive.confirm_value(
        "replica_region", replica_region.value, "Replica region (auto)"
    )
    environment = interactive.prompt_enum(
        models.Environment,
        "environment",
        "What type of deployment is this?",
        models.Environment.dev,
    )
    is_prod_like = environment == models.Environment.production
    matrix_domain = interactive.prompt_text(
        "matrix_domain",
        description="What will the domain be for this keanu instance? e.g., matrix.org",
        validator=validators.validate_domain,
    )
    suggested_name = matrix_domain[0 : matrix_domain.rfind(".")].title()
    name = interactive.prompt_text(
        "name",
        description="What is the name of this instance that users will refer to it by? Example: Keanu, Circulo ",
        default=suggested_name,
    )

    name_identifier = name.lower().replace(" ", "-")

    namespace = util.abbreviate(name_identifier, 4)

    aws_profile = f"keanu-{name_identifier}"

    maus = interactive.prompt_options(
        "active users",
        ["10", "100", "1000", "10000"],
        description="How many monthly active users will there be?",
    )
    has_search = interactive.prompt_yesno(
        "room and user search", description="Should rooms and users be searchable?"
    )
    federation_enabled = interactive.prompt_yesno(
        "federation enabled",
        description="Should the instance federate with the wider matrix ecosystem?",
    )
    s3_enabled = interactive.prompt_yesno(
        "s3 media storage", description="Should the instance store its media in S3?"
    )
    registration_open = interactive.prompt_yesno(
        "open registration", description="Should registration be open to the public?"
    )

    sygnal_enabled = interactive.prompt_yesno(
        "sygnal_enabled",
        description="Does this server need to serve push notifications?",
    )
    ma1sd_enabled = interactive.prompt_yesno(
        "ma1sd_enabled",
        description="Should this server use its own isolated identity server?",
    )

    if ma1sd_enabled:
        url = interactive.prompt_text(
            "homepage",
            description="What is the URL to the homepage of this instance?",
            validator=validators.validate_url,
        )

    instance_size = interactive.choose_instance_size(federation_enabled, maus)
    email_from_address = f"noreply@{matrix_domain}"
    kc = {
        "base_config": {
            "namespace": namespace,
            "environment": environment,
            "aws_profile": aws_profile,
            "region": region,
            "replica_region": replica_region,
            "az1": az1,
            "az2": az2,
            "project": name,
            "is_prod_like": is_prod_like,
        },
        "synapse_config": {
            "federation_enabled": federation_enabled,
            "registration_enabled": registration_open,
            "user_directory_enabled": has_search,
            "enable_room_list_search": has_search,
            "s3_media_store_enabled": s3_enabled,
            "s3_media_store_region": region,
        },
        "ma1sd_config": {
            "email_from_address": email_from_address,
            "email_from_name": name,
            "ma1sd_policies": [
                {
                    "id": util.random_identifier(8),
                    "terms": [
                        {
                            "language_code": LanguageCode.en,
                            "name": "Terms Of Service",
                            "url": url,
                        }
                    ],
                    "version": "1.0",
                }
            ],
        },
        "matrix_instance_type": instance_size,
        "sygnal_enabled": sygnal_enabled,
        "ma1sd_enabled": ma1sd_enabled,
        "matrix_domain": matrix_domain,
        "matrix_server_fqdn": f"matrix.{matrix_domain}",
        "web_domain": f"web.{matrix_domain}",
        "weblite_domain": f"lite.{matrix_domain}",
        "rds_db_name": name_identifier.replace("-", "_"),
        "email_from_address": email_from_address,
        "friendly_name": name,
        "id": name_identifier,
    }
    return kc
