import json

import yaml

from keanu_conf.models import AWSBaseConfig, KeanuConfig


def to_terraform(config):
    locals = {}
    for field, value in config.items():
        locals[field] = value
    project = {"locals": locals}
    return project


def load_yaml(config_file):
    with open(config_file, "r") as f:
        return yaml.safe_load(f)


def load_config(config_file):
    doc = load_yaml(config_file)
    return AWSBaseConfig(**doc)


def serialize_terraform(tf):
    return json.dumps(tf, indent=2, sort_keys=True)


def prompt(field, props):
    if "description" in props:
        print(props["description"])

    prompt = f"{field}"
    has_default = "default" in props
    while True:
        if has_default:
            prompt += f" ({props['default']})"
        result = input(f"{prompt}: ").strip()
        if len(result) == 0:
            if has_default:
                return props["default"]
        else:
            return result


def generate():
    keanu_input = {}
    schema = KeanuConfig.schema()
    print(schema)
    print()
    for field, props in schema["properties"].items():
        keanu_input[field] = prompt(field, props)
        print()


def main():
    c = load_config("project.yml")
    tf = to_terraform(c.dict())
    json_doc = serialize_terraform(tf)
    print(json_doc)
