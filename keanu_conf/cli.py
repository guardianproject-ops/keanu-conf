import os
import pathlib

import click

from keanu_conf import AbstractConfiguration, Context, configs, interactive, keanu

avilable_connfigurations = {"keanu": keanu.KeanuConfiguration}


@click.group()
@click.option(
    "--ops-root",
    required=True,
    help="The path to the operations root folder",
    type=click.Path(exists=True),
    default=os.path.realpath("/workspace/guardianproject-ops/ops"),
)
@click.option("--debug/--no-debug", default=False, envvar="DEBUG")
@click.pass_context
def cli(ctx, ops_root, debug):
    ctx.obj = Context(pathlib.Path(ops_root), debug)
    pass


@cli.command(help="Create a new configuration")
@click.option(
    "--type",
    default="keanu",
    type=click.Choice(configs.get_configuration_type_names()),
    help="The type of configuration",
)
@click.pass_obj
def create(ctx, type):
    config_type = None
    if type is None:
        config_type = interactive.choose_configuration_type()
    else:
        config_type = configs.get_available_configurations_types()[type]
    config = config_type(ctx)
    config.create_interactive()


def _load(ctx: Context, type: str, id: str) -> AbstractConfiguration:
    ctor = configs.get_available_configurations_types()[type]
    config = ctor(ctx, id)
    config.load()
    return config


@cli.command(help="load")
@click.option(
    "--type",
    default="keanu",
    type=click.Choice(configs.get_configuration_type_names()),
    help="The type of configuration",
)
@click.argument("id")
@click.pass_obj
def load(ctx: Context, type: str, id: str) -> None:
    _load(ctx, type, id)


@cli.command
@click.argument("id")
@click.option(
    "--type",
    default="keanu",
    type=click.Choice(configs.get_configuration_type_names()),
    help="The type of configuration",
)
@click.pass_obj
def tf_plan(ctx: Context, type: str, id: str):
    deployment = _load(ctx, type, id)
    deployment.initialize_remote_state()


if __name__ == "__main__":
    cli()
