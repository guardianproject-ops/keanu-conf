# Copyright 2021-2022 SR2 Communications Limited.
# Licensed under BSD-2-Clause. See LICENSE.BSD.SR2
import pathlib
from abc import ABCMeta, abstractmethod
from typing import Any, Optional, Tuple

import jinja2


class BaseAutomation(metaclass=ABCMeta):
    short_name: str = "base"
    """
    The short name of the automation provider. This is used as an opaque token throughout
    the portal system.
    """
    description: str = "Abstract base automation."
    frequency: int
    cwd: pathlib.Path
    """
    Provides a filesystem path that can be used during the automation run.
    This is currently a persistent path, but this should not be relied upon
    as future versions may use disposable temporary paths instead. State that
    is needed in subsequent runs should be stored elsewhere.

    :param filename: the filename inside the working directory to create a path for
    :return: filesystem path for that filename
    """

    @abstractmethod
    def __init__(self, working_directory: pathlib.Path):
        super(BaseAutomation, self).__init__()
        self.cwd = working_directory

    @abstractmethod
    def automate(self, full: bool = False) -> Tuple[bool, str]:
        raise NotImplementedError()

    def working_directory(self, filename: Optional[str] = None) -> pathlib.Path:
        """
        Provides a filesystem path that can be used during the automation run.
        This is currently a persistent path, but this should not be relied upon
        as future versions may use disposable temporary paths instead. State that
        is needed in subsequent runs should be stored elsewhere.

        :param filename: the filename inside the working directory to create a path for
        :return: filesystem path for that filename
        """
        return self.cwd.joinpath(
            self.short_name or self.__class__.__name__.lower()
        ).joinpath(filename or "")

    def tmpl_write(self, filename: str, template: str, **kwargs: Any) -> None:
        """
        Write a Jinja2 template to the working directory for use by an automation module.

        :param filename: filename to write to
        :param template: Jinja2 template
        :param kwargs: variables for use with the template
        :return: None
        """
        tmpl = jinja2.Template(template)
        with open(self.working_directory(filename), "w", encoding="utf-8") as tfconf:
            tfconf.write(tmpl.render(**kwargs))
