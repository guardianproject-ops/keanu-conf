from typing import Any, List

from keanu_conf.terraform.terraform import TerraformAutomation


class RemoteStateAutomation(TerraformAutomation):
    short_name = "remote_state"
    description = "Setup terraform state resources (S3, dynamodb table, etc)"
    provider = "aws"

    template: str
    template_parameters: List[str] = [
        "region",
        "replica_region",
        "profile",
        "aws_account_id",
        "namespace",
        "environment",
        "project",
    ]
    """
    List of parameters to be read from the application configuration for use
    in the templating of the Terraform configuration.
    """

    template = """
provider "aws" {
  region = "{{ region }}"
  profile = "{{ profile }}"
  allowed_account_ids = ["{{ aws_account_id }}"]
}

provider "aws" {
  alias   = "replica"
  region = "{{ replica_region }}"
  profile = "{{ profile }}"
  allowed_account_ids = ["{{ aws_account_id }}"]
}

module "remote_state" {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-modules-operations//remote-state?ref=master"
  config = {
    namespace      = "{{ namespace }}"
    environment    = "{{ environment }}"
    region         = "{{ region }}"
    replica_region = "{{ replica_region }}"
    aws_profile    = "{{ profile }}"
    tags           = {
        "Project": "{{ project }}"
    }
  }
  providers = {
    aws = aws
    aws.replica = aws.replica
  }
}

locals {
  backend_tf = <<EOF
  terraform {
  backend "s3" {
    bucket         = "${module.remote_state.config.state_bucket.id}"
    key            = "${module.remote_state.state_key_prefix}/terraform.tfstate"
    region         = "{{ region }}"
    encrypt        = true
    kms_key_id     = "${module.remote_state.config.kms_key.arn}"
    dynamodb_table = "${module.remote_state.config.dynamodb_table.name}"
    profile        = "{{ profile }}"
  }
}
EOF

}

output "remote_state" {
  value = module.remote_state.config
}

output "backend_tf" {
  value = local.backend_tf
}"""

    """
    Terraform configuration template using Jinja 2.
    """

    def tf_generate(self, values: Any = {}) -> None:
        self.working_directory().mkdir(exist_ok=True)
        self.tf_write(self.template, **values)
