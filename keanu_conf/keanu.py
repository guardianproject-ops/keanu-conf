from typing import Optional, Type

from pydantic import BaseModel

from keanu_conf import AbstractConfiguration, interactive, keanu_prompts, models
from keanu_conf.terraform.remote_state import RemoteStateAutomation


class KeanuConfiguration(AbstractConfiguration):
    short_name = "keanu"
    description = "Deploy the keanu matrix stack"

    root_config: Optional[models.KeanuConfig]

    @property
    def root_config_type(self) -> Type[models.BaseModel]:
        return models.KeanuConfig

    def prompt(self) -> models.KeanuConfig:
        interactive.print_title(f"Creating new {self.short_name} configuration")
        kc = keanu_prompts.preset_questions_menu()  # type: ignore[no-untyped-call]
        return interactive.prompt_model_loop(
            models.KeanuConfig,
            prefilled=kc,
            prompt_for_defaults=False,
            skip_prefilleds=True,
        )

    def create_interactive(self) -> BaseModel:
        self.root_config = self.prompt()
        self.id = self.root_config.id
        self.persist()
        print(f"Created {self.id} configuration at {self.config_dir}")
        return self.root_config

    def initialize_remote_state(self) -> None:
        assert self.root_config
        base_config = self.root_config.base_config
        remote_state = RemoteStateAutomation(self.deployment_dir)
        remote_state.tf_generate(
            {
                "region": base_config.operations_region.value,
                "replica_region": base_config.replica_region.value,
                "profile": base_config.operations_aws_profile,
                "aws_account_id": base_config.operations_account_id,
                "namespace": base_config.namespace,
                "environment": base_config.environment.value,
                "project": base_config.project,
            }
        )
        remote_state.tf_init()
        remote_state.tf_plan()
