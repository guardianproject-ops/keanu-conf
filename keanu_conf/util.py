import io
import itertools
import secrets
import string

from signedjson.key import generate_signing_key, write_signing_keys


def generate_password(length=30, alphabet=string.ascii_letters + string.digits) -> str:
    return "".join(secrets.choice(alphabet) for i in range(length))


def random_identifier(length: int) -> str:
    return "".join(secrets.choice(string.ascii_letters) for _ in range(length))


def generate_synapse_signing_key() -> str:
    key_id = "a_" + random_identifier(4)
    with io.StringIO() as f:
        write_signing_keys(
            f,
            (generate_signing_key(key_id),),
        )
        f.seek(0)
        return f.read()


def abbreviate(s: str, size: int) -> str:
    # special cases
    if len(s) <= size:
        return s
    if size == 2:
        return s[0] + s[-1]
    elif size == 1:
        return s[0]
    elif size == 0:
        return ""

    # remove vowels one by one
    vowels_forever = itertools.cycle("aeiou")
    result = s
    while len(result) > size:
        result = result.replace(next(vowels_forever), "", 1)
        if not any(vowel in result for vowel in "aeiou"):
            break

    # remove other characters one by one
    # preserve the first and last letter
    chars = list(result)
    middle = chars[2:-1]
    while len(middle) > (size - 2):
        middle.pop(0)
    return chars[0] + "".join(middle) + chars[-1]
