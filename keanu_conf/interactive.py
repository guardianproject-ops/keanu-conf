# mypy: disallow_untyped_calls=False
import copy
import enum
import pprint
from functools import partial
from typing import Optional, Type, TypeVar

import pydantic
from colored import fg, stylize_interactive
from pydantic import ValidationError
from simple_term_menu import TerminalMenu
from typing_extensions import TypedDict

from keanu_conf import AbstractConfiguration, configs, models, models_aws
from keanu_conf.models_aws import Region

pp = pprint.PrettyPrinter(indent=4)

Model = TypeVar("Model", bound=pydantic.BaseModel)


colorify = stylize_interactive


def special_options(interactive_options, gathered, field_name, starting_options):
    """
    A special hook that allows an enum field to alter its options based
    on the current state of the gathered fields
    """
    options = starting_options
    for opt_name in interactive_options:
        if opt_name == "replica_region":
            # replica region and region can't be the same, so remove the option to choose the region value
            # when showing replica_region
            if "region" in gathered:
                options = [o for o in options if o != gathered["region"]]
        elif opt_name == "az":
            options = [az.value for az in Region(gathered["region"]).zones]
        elif opt_name == "az2_dupe":
            if "az1" in gathered:
                options = [
                    az.value
                    for az in Region(gathered["region"]).zones
                    if az != gathered["az1"]
                ]
    return options


def build_prompt(name, description=None, default=None, indent=0):
    if description:
        print(colorify(f"{description}", fg("yellow")))

    spacer = " " * indent * 4
    prompt = colorify(f"{spacer}{name}", fg("green"))
    if default:
        prompt += f" ({default})"
    return prompt


def prompt_text(
    name, validator=None, description="", default=None, indent=0, allow_empty=False
):
    prompt = build_prompt(name, description, default, indent)
    while True:
        result = input(f"{prompt}: ").strip()
        is_empty = len(result) == 0
        if is_empty and allow_empty:
            if default:
                return default
            else:
                return result
        elif is_empty and default:
            return default
        elif validator:
            try:
                return validator(result)
            except ValueError as e:
                print(e)
        else:
            return result


def prompt_options(name, opts, validator=None, description=None, default=None):
    finished = False
    while not finished:
        initial_index = 0
        if default in opts:
            initial_index = opts.index(default)
        menu = TerminalMenu(
            opts,
            title=f"{name}: ",
            status_bar=description,
            cursor_index=initial_index,
            raise_error_on_interrupt=True,
        )
        index = menu.show()
        if index is not None:
            result = opts[index]
            if validator:
                try:
                    result = validator(result)
                    finished = True
                except ValueError as e:
                    print(e)
            else:
                finished = True
    confirm_value(name, result, description)
    return result


def prompt_yesno(name, validator=None, description=None, default=None):
    opts = ["[y] yes", "[n] no"]
    finished = False
    while not finished:
        initial_index = 0
        if default is False:
            initial_index = 1
        menu = TerminalMenu(
            opts,
            title=f"{name}: ",
            status_bar=description,
            cursor_index=initial_index,
            raise_error_on_interrupt=True,
        )
        index = menu.show()
        if index is not None:
            result = True if index == 0 else False
            if validator:
                try:
                    result = validator(result)
                    finished = True
                except ValueError as e:
                    print(e)
            else:
                finished = True
    confirm_value(name, "yes" if result else "no", description)
    return result


def prompt_bool(name, validator=None, description=None, default=None):
    finished = False
    opts = ["[t] true", "[f] false"]
    while not finished:
        initial_index = 0
        if default is False:
            initial_index = 1
        menu = TerminalMenu(
            opts,
            title=f"{name}: ",
            status_bar=description,
            cursor_index=initial_index,
            raise_error_on_interrupt=True,
        )
        index = menu.show()
        result = True if index == 0 else False
        if validator:
            try:
                result = validator(result)
                finished = True
            except ValueError as e:
                print(e)
        else:
            finished = True
    confirm_value(name, result, description)
    return result


def prompt_dict(name, validator=None, description=None, default=None):
    prompt = build_prompt(name, description)
    print(prompt)
    result = {}
    print("(leave empty to complete)")
    while True:
        key = prompt_text("key", indent=1, allow_empty=True)
        if not key:
            return result
        value = prompt_text("value", indent=1, allow_empty=True)
        result[key] = value
        print()


def prompt_list(name, validator=None, description=None, default=None):
    prompt = build_prompt(name, description)
    print(prompt)
    result = []
    print("(leave empty to complete)")
    while True:
        current_default = None
        if default and len(default) > len(result):
            current_default = default[len(result)]
        value = prompt_text(
            f"{name}[{len(result)}]",
            indent=1,
            allow_empty=True,
            default=current_default,
        )
        if not value:
            value = current_default
        if not current_default and not value:
            return result

        result.append(value)


def make_typeddict_model(
    typeddict_cls: Type[TypedDict], config: Type[pydantic.config.BaseConfig]  # type: ignore[valid-type]
) -> Type[pydantic.BaseModel]:
    from pydantic.annotated_types import create_model_from_typeddict

    TypedDictModel = create_model_from_typeddict(
        typeddict_cls,
        __config__=config,
        __module__=typeddict_cls.__module__,
    )
    typeddict_cls.__pydantic_model__ = TypedDictModel  # type: ignore[attr-defined]
    typeddict_cls.__doc__ = typeddict_cls.__doc__
    return TypedDictModel


def prompt_typeddict(field, default):
    sub_model = make_typeddict_model(field.type_, None)
    # pp.pprint(sub_model.schema())
    return prompt_model(
        sub_model,
        default if default else {},
        prompt_for_defaults=True,
        skip_prefilleds=False,
        field_prompt=FieldPrompt.BOTH,
    )


def pretty_print_dict(d, indent=1):
    for k, v in d.items():
        spacer = " " * indent * 4
        pretty_key = colorify(f"{spacer}{k}", fg("green"))
        pretty_value = str(v)
        print(f"{pretty_key}: {pretty_value}")


def prompt_complex_list(field, validator, description, default):
    name = field.name
    prompt = build_prompt(name, description)
    print(prompt)
    result = []
    print("(leave empty to complete)")
    while True:
        current_default = None
        if default and len(default) > len(result):
            current_default = default[len(result)]
        if pydantic.typing.is_typeddict(field.type_):
            value = prompt_typeddict(field, default)
        elif is_model(field):
            value = prompt_model(
                field.type_,
                current_default if current_default else {},
                prompt_for_defaults=False,
            )
        else:
            raise NotImplementedError()
        if not value:
            value = current_default
        if not current_default and not value:
            return result

        result.append(value)

        for i, r in enumerate(result):
            print(colorify(f"{name}[{i}]:", fg("green")))
            pretty_print_dict(r)
        print()
        add_another = prompt_yesno("add another?")
        if not add_another:
            return result


def confirm_value(name, value, description):
    if description:
        print(stylize_interactive(f"{description}", fg("yellow")))

    prompt = stylize_interactive(f"{name}", fg("green"))
    print(f"{prompt}: {value}")


def find_field(errors, field):
    for e in errors:
        if field in e["loc"]:
            return e
    return None


def handle_errors(errors, field):
    error = find_field(errors, field)
    if not error:
        return None
    return error


def validate_field(model, gathered, field_name, value):
    doc = copy.deepcopy(gathered)
    doc[field_name] = value
    # pp.pprint(doc)
    try:
        model(**doc)
        return value
    except ValidationError as e:
        # pp.pprint(e.errors())
        error_result = handle_errors(e.errors(), field_name)
        if not error_result:
            return value
        else:
            # pp.pprint(error_result)
            raise ValueError(error_result["msg"])


class FieldPrompt(enum.Enum):
    ONLY_NORMAL = 1
    ONLY_ADVANCED = 2
    BOTH = 3


def is_enum(field):
    return (
        field.shape == pydantic.fields.SHAPE_SINGLETON
        and type(field.type_) == enum.EnumMeta
    )


def is_str(field):
    return (
        field.shape == pydantic.fields.SHAPE_SINGLETON
        and (field.type_ == str or issubclass(field.type_, str))
        and not is_enum(field)
    )


def is_bool(props):
    return "type" in props and props["type"] == "boolean"


def is_dict(field):
    return field.shape == pydantic.fields.SHAPE_DICT


def is_complex(field):
    return field.is_complex()


def is_list(field):
    return field.shape == pydantic.fields.SHAPE_LIST


def is_model(field):
    return pydantic.utils.lenient_issubclass(field.type_, pydantic.BaseModel)


def is_singleton_model(field):
    return is_model(field) and field.shape == pydantic.fields.SHAPE_SINGLETON


def is_int(field):
    return field.shape == pydantic.fields.SHAPE_SINGLETON and field.type_ == int


def build_enum_options(gathered, props, field):
    opts = [d.value for d in field.type_]
    interactive_options = props.get("interactive_options", None)
    if interactive_options:
        opts = special_options(interactive_options, gathered, field.name, opts)
    return opts


def resolve_enum(field, str_value):
    return field.type_(str_value)


def prompt_field(
    model,
    gathered,
    field,
    props,
    pre_filled_field,
    prompt_for_defaults=True,
    skip_prefilleds=False,
    field_prompt=FieldPrompt.BOTH,
):
    name = field.name
    description = props.get("description", None)
    is_generated = props.get("is_generated", False)
    validator = partial(validate_field, model, gathered, name)

    # pp.pprint(f"prompting {field.name}")
    # pp.pprint(gathered)
    if is_singleton_model(field):
        return prompt_model(
            field.type_,
            pre_filled_field if pre_filled_field else {},
            prompt_for_defaults,
            skip_prefilleds,
            field_prompt=field_prompt,
        )

    default = props.get("default", None)
    if name in gathered:
        # already filled in
        if skip_prefilleds:
            return gathered[name]
    elif default is not None and not prompt_for_defaults:
        return default

    if name in gathered and not skip_prefilleds:
        default = gathered[name]

    if is_str(field) or is_int(field):
        return prompt_text(
            name, validator, description, default, allow_empty=is_generated
        )
    elif is_enum(field):
        if isinstance(default, enum.Enum):
            default = default.value
        options = build_enum_options(gathered, props, field)
        enum_str = prompt_options(name, options, validator, description, default)
        v = resolve_enum(field, enum_str)
        return v
    elif is_bool(props):
        v = prompt_bool(name, validator, description, default)
        return v
    elif is_dict(field):
        v = prompt_dict(name, validator, description, default)
        return v
    elif is_list(field):
        if is_complex(field):
            v = prompt_complex_list(field, validator, description, default)
        else:
            v = prompt_list(name, validator, description, default)
        return v
    else:
        raise ValueError(f"Unhandled field type {field.type_}")


def prompt_fields(
    model,
    initial_result,
    fields,
    prompt_for_defaults=True,
    skip_prefilleds=False,
    field_prompt=FieldPrompt.BOTH,
):
    result = copy.deepcopy(initial_result)
    for field_name, props in fields:
        field = model.__fields__[field_name]

        # pp.pprint("------ PROCESSING FIELD -----------")
        # pp.pprint("+++ field: ")
        # pp.pprint(field)
        # pp.pprint("+++ props: ")
        # pp.pprint(props)
        # pp.pprint("+++ prefilled: ")
        # pp.pprint(initial_result)
        # pp.pprint("------ END PROCESSING FIELD -----------")
        prefilled_field = initial_result.get(field_name, None)
        result[field_name] = prompt_field(
            model,
            result,
            field,
            props,
            prefilled_field,
            prompt_for_defaults,
            skip_prefilleds,
            field_prompt=field_prompt,
        )
        # pp.pprint("------ RESULT SO FAR -----------")
        # pp.pprint(result)
        # pp.pprint("------ END RESULT SO FAR -----------")
    return result


def load_fields(schema):
    schema_items = schema["properties"].items()
    normal = [
        (name, props)
        for name, props in schema_items
        if "advanced" not in props or not props["advanced"]
    ]
    advanced = [
        (name, props)
        for name, props in schema_items
        if "advanced" in props and props["advanced"]
    ]
    return normal, advanced


def prompt_model(
    model: type[Model],
    prefilled={},
    prompt_for_defaults=True,
    skip_prefilleds=False,
    field_prompt: FieldPrompt = FieldPrompt.BOTH,
):
    schema = model.schema()
    # description = schema["description"] if "description" in schema else ""
    # print(colorify(description, fg("blue")))
    normal_fields, advanced_fields = load_fields(schema)

    def _prompt_fields(field_set):
        return prompt_fields(
            model,
            prefilled,  # {},
            field_set,
            prompt_for_defaults=prompt_for_defaults,
            skip_prefilleds=skip_prefilleds,
            field_prompt=field_prompt,
        )

    if field_prompt == FieldPrompt.ONLY_NORMAL:
        result = _prompt_fields(normal_fields)
    elif field_prompt == FieldPrompt.ONLY_ADVANCED:
        result = _prompt_fields(advanced_fields)
    elif field_prompt == FieldPrompt.BOTH:
        result = _prompt_fields(normal_fields + advanced_fields)

    # pp.pprint(result)
    return result


def prompt_model_loop(
    model: Type[Model], prefilled=None, prompt_for_defaults=True, skip_prefilleds=False
) -> Model:
    print()
    description = model.schema()["description"]
    raw_result = prompt_model(
        model,
        prefilled,
        prompt_for_defaults,
        skip_prefilleds,
        field_prompt=FieldPrompt.ONLY_NORMAL,
    )
    final_model = model.parse_obj(raw_result)
    edit_loop = True
    while edit_loop:
        menu = TerminalMenu(
            menu_entries=[
                "Continue",
                "Edit Basic Config Again",
                "Edit Advanced Config",
            ],
            title=description,
            raise_error_on_interrupt=True,
        )
        r = menu.show()
        if r == 0:  # Continue
            edit_loop = False
        elif r == 1:  # Basic Again
            raw_result = prompt_model(
                model,
                final_model.dict(),
                prompt_for_defaults=True,
                skip_prefilleds=False,
                field_prompt=FieldPrompt.ONLY_NORMAL,
            )
            final_model = model.parse_obj(raw_result)
        elif r == 2:  # Advanced
            print(colorify(description, fg("blue")) + " :: Advanced")
            raw_result = prompt_model(
                model,
                final_model.dict(),
                prompt_for_defaults=True,
                skip_prefilleds=False,
                field_prompt=FieldPrompt.BOTH,
            )
            final_model = model.parse_obj(raw_result)

    return final_model
    raise ValueError("uhoh")


def choose_instance_size(federation_enabled, maus):
    if federation_enabled:
        if maus == "10":
            return models.Ec2InstanceTypes.t3_large
        elif maus == "100":
            return models.Ec2InstanceTypes.m5_large
        elif maus == "1000":
            return models.Ec2InstanceTypes.m5_xlarge
        elif maus == "10000":
            return models.Ec2InstanceTypes.m5_2xlarge
    else:
        if maus == "10":
            return models.Ec2InstanceTypes.t3_small
        elif maus == "100":
            return models.Ec2InstanceTypes.t3_large
        elif maus == "1000":
            return models.Ec2InstanceTypes.m5_large
        elif maus == "10000":
            return models.Ec2InstanceTypes.m5_xlarge


def prompt_enum(enum, name, description, default=None):
    options = [d.value for d in enum]
    v = prompt_options(name, options, None, description, default)
    confirm_value(name, v, description)
    return enum(v)


def prompt_region():
    options = [d.value for d in models_aws.preferred_regions]
    initial_index = options.index(models_aws.default_region.value)
    options.append("View All Regions")
    name = "region"
    description = "Which region should we deploy to?"
    menu = TerminalMenu(
        menu_entries=options,
        title=name,
        status_bar=description,
        cursor_index=initial_index,
        raise_error_on_interrupt=True,
    )
    r = None
    while r is None:
        r = menu.show()

    if r == len(options) - 1:
        return prompt_enum(models.Region, name, description, models_aws.default_region)

    confirm_value(name, options[r], description)
    return Region(options[r])


def replica_region_auto(region):
    return [d.value for d in models.Region if d.value != region][0]


def choose_configuration_type() -> Optional[Type[AbstractConfiguration]]:
    available = configs.get_available_configurations_types()
    entries = [c for c in available.keys()]
    entries.append("Quit")
    menu = TerminalMenu(
        title="Configurator 3000 :: Choose a Configuration Type",
        menu_entries=entries,
        cycle_cursor=True,
        clear_screen=True,
        raise_error_on_interrupt=True,
    )
    main_menu_exit = False
    result = None
    while not main_menu_exit:
        r = menu.show()
        if r == len(entries) - 1:
            print("Goodbye")
            main_menu_exit = True
        else:
            result = available[entries[r]]
            main_menu_exit = True
    return result


def print_title(text: str):
    print(text, fg("yellow"))
