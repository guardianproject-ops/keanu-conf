# mypy: disallow_untyped_calls=False
import enum
import json
import pkgutil


def _load_regions():
    return json.loads(pkgutil.get_data(__name__, "data/regions.json"))


def _get_regions(region_data):
    r = {}
    for region in region_data:
        if region["public"]:
            key = region["code"].replace("-", "_")
            r[key] = region["code"]
    return r


def _get_azs(region_data):
    r = {}
    for region in region_data:
        for az in region["zones"]:
            key = az.replace("-", "_")
            r[key] = az
    return r


def _azs_by_region(region_data):
    r = {}
    for region in region_data:
        r[region["code"]] = region["zones"]
    return r


def _build_enums():
    data = _load_regions()
    azs = _azs_by_region(data)

    Region = enum.Enum("Region", _get_regions(data))
    AvailabilityZone = enum.Enum("AvailabilityZone", _get_azs(data))

    for region in Region:
        region.zones = []
        for az_code in azs[region.value]:
            region.zones.append(AvailabilityZone(az_code))
    return Region, AvailabilityZone


def valid_az(region, maybe_az):
    for zone in region.zones:
        if zone == maybe_az or zone.value == maybe_az:
            return True
    return False


Region, AvailabilityZone = _build_enums()

default_region = Region.eu_central_1
preferred_regions = [Region.us_east_2, Region.eu_west_1, Region.eu_central_1]
