# from dataclasses import dataclass, fields
import pprint
from enum import Enum
from typing import Dict, List, Optional

from pydantic import BaseModel, EmailStr, Field, HttpUrl, validator
from typing_extensions import Annotated, TypedDict

from keanu_conf import util, validators
from keanu_conf.models_aws import AvailabilityZone, Region, valid_az
from keanu_conf.models_language import LanguageCode

pp = pprint.PrettyPrinter(indent=4)


# class BaseModel(PydanticBaseModel):
#     @classmethod
#     def get_field_names(cls, alias=False):
#         return list(cls.schema(alias).get("properties").keys())


class Environment(str, Enum):
    production = "prod"
    dev = "dev"
    test = "test"


class Ec2InstanceTypes(str, Enum):
    t3_small = "t3.small"
    t3_medium = "t3.medium"
    t3_large = "t3.large"
    t3_xlarge = "t3.xlarge"
    t3_2xlarge = "t3.2xlarge"
    m5_large = "m5.large"
    m5_xlarge = "m5.xlarge"
    m5_2xlarge = "m5.2xlarge"
    m5_4xlarge = "m5.4xlarge"
    m6a_large = "m6a.large"
    m6a_xlarge = "m6a.xlarge"
    m6a_2xlarge = "m6a.2xlarge"
    m6a_4xlarge = "m6a.4xlarge"


class RdsInstanceTypes(str, Enum):
    db_t3_small = "db_t3_small"
    db_t3_medium = "db_t3_medium"
    db_t3_large = "db_t3_large"
    db_t3_xlarge = "db_t3_xlarge"
    db_t3_2xlarge = "db_t3_2xlarge"
    db_m5_large = "db_m5_large"
    db_m5_xlarge = "db_m5_xlarge"
    db_m5_2xlarge = "db_m5_2xlarge"
    db_m5_4xlarge = "db_m5_4xlarge"


class AWSBaseConfig(BaseModel):
    """AWS Configuration"""

    namespace: str = Field(
        ..., description="The deployment namespace", min_length=1, max_length=6
    )
    environment: Environment = Field(
        Environment.dev, description="The type of environment"
    )
    project: str = Field(
        ...,
        description="A short identifier for this deployment",
        min_length=1,
        max_length=6,
    )
    is_prod_like: bool = Field(
        ..., description="Whether or not this is a production deployment"
    )
    region: Region = Field(
        ..., description="The AWS region this deployment will live in"
    )
    az1: Optional[AvailabilityZone] = Field(
        description="The primary availability zone",
        interactive_options=["az"],
    )
    az2: Optional[AvailabilityZone] = Field(
        description="The secondary availability zone",
        interactive_options=["az", "az2_dupe"],
    )
    replica_region: Region = Field(
        ...,
        description="The AWS region for replications/HA",
        interactive_options=["replica_region"],
    )
    aws_profile: str = Field(
        ..., description="The AWS cli profile to authenticate with"
    )
    extra_tags: Dict[str, str] = Field(
        {},
        description="Any extra tags to be applied to all resources",
        advanced=True,
    )
    account_id: str = Field(
        ...,
        description="The AWS account id this deployment will live in",
        regex=r"^\d{12}",
    )
    operations_account_id: str = Field(
        "270065562873",
        description="The operations account id for shared resources",
        regex=r"^\d{12}",
        advanced=True,
    )
    operations_region: Region = Field(
        Region.eu_west_1,
        description="The AWS region the operations account's shared resources are in",
        advanced=True,
    )
    operations_aws_profile: str = Field(
        "gp-operations",
        description="The AWS cli profile to authenticate to the operations account with",
        advanced=True,
    )

    @validator("region", pre=True, always=True)
    def default_region(value, values, config, field):
        other = "replica_region"
        if other in values and values[other] == value:
            raise ValueError("replica_region and region cannot be the same")
        return value

    @validator("replica_region", pre=True, always=True)
    def default_replica_region(value, values, config, field):
        other = "region"
        if other in values and values[other] == value:
            raise ValueError("replica_region and region cannot be the same")
        return value

    @validator("az1", pre=True, always=True)
    def default_az1(cls, value, values, **kwargs):
        if valid_az(values["region"], value):
            return value
        else:
            raise ValueError(
                f"The availability zone {value} is not in the region {values['region']}"
            )

    @validator("az2", pre=True, always=True)
    def default_az2(cls, value, values, **kwargs):
        if valid_az(values["region"], value):
            return value
        else:
            raise ValueError(
                f"The availability zone {value} is not in the region {values['region']}"
            )


class RoomEncryptionType(str, Enum):
    all = "all"
    invite = "invite"
    off = "off"


class SynapseConfig(BaseModel):
    """
    Configuration for Synapse Homeserver
    """

    macaroon_secret_key: Annotated[
        str,
        Field(
            sensitive=True,
            is_generated=True,
            min_length=20,
            advanced=True,
            description="",
            default_factory=lambda: util.generate_password(),
        ),
    ]
    password_config_pepper: Annotated[
        str,
        Field(
            sensitive=True,
            is_generated=True,
            min_length=20,
            advanced=True,
            description="",
            default_factory=lambda: util.generate_password(),
        ),
    ]
    signing_key: Annotated[
        str,
        Field(
            sensitive=True,
            is_generated=True,
            advanced=True,
            description="",
            default_factory=lambda: util.generate_synapse_signing_key(),
        ),
    ]

    _validate_signing_key = validator("signing_key", allow_reuse=True)(
        validators.validate_signing_key
    )

    admin_contact_email: EmailStr = Field(
        ...,
        description="The email address for contacting the matrix server owner in case of abuse or complaints",
    )
    enable_room_list_search: bool = Field(
        False,
        description="If true, then public rooms on the matrix server can be found by anyone",
    )
    user_directory_enabled: bool = Field(
        False,
        description="If true, then users can find other users using the user search feature",
    )
    encryption_enabled_by_default_for_room_type: RoomEncryptionType = Field(
        RoomEncryptionType.invite,
        description="Controls whether locally-created rooms should be end-to-end encrypted by default.",
    )
    manhole_enabled: bool = Field(
        False,
        description="If the manhole is enabled or not",
        advanced=True,
    )

    registration_enabled: bool = Field(
        True, description="If true, registration will be open to the public"
    )
    federation_enabled: bool = Field(
        False, description="Whether or not federation will be enabled"
    )
    federation_domain_whitelist: List[str] = Field(
        [],
        description="The other matrix servers this deployment will be allowed to federate with",
    )
    s3_media_store_enabled: bool = Field(
        True, description="If true, media will be stored in s3"
    )
    s3_media_store_region: Region = Field(
        ..., description="The region media will be stored in in s3"
    )

    @validator("s3_media_store_region", pre=True, always=True)
    def default_s3_media_store_regio(value, values, config, field):
        if values["s3_media_store_enabled"] and not value:
            raise ValueError(
                "s3_media_store_enabled is true, so a region must be specified"
            )
        return value


class Ma1sdPolicyTerm(TypedDict):
    """Ma1sd Policy Term"""

    language_code: Annotated[
        LanguageCode, Field(description="The language of the policies")
    ]
    name: Annotated[str, Field(description="The friendly name of the terms of service")]
    url: Annotated[HttpUrl, Field(description="A url to the terms of service policy")]


class Ma1sdPolicyContainer(BaseModel):
    """Ma1sd Policy Container"""

    version: str = Field("1.0", description="terms version number")
    id: str = Field(
        description="A unique identifier for this policy",
        is_generated=True,
        advanced=True,
        default_factory=lambda: util.random_identifier(6),
    )
    terms: List[Ma1sdPolicyTerm] = Field(
        ...,
        description="The list of policy terms in different languages",
    )


class Ma1sdConfig(BaseModel):
    """Ma1sd Identity Server"""

    ma1sd_policies: List[Ma1sdPolicyContainer] = Field(
        ..., description="The identity server terms of service policies"
    )
    email_from_address: EmailStr = Field(
        ...,
        description="The email address that automated emails will come from. Default: noreply@<matrix_domain>",
    )
    email_from_name: str = Field(
        ...,
        description="The name of the sender that appears in emails: Default: FriendlyName",
    )


class KeanuConfig(BaseModel):
    """Keanu Matrix"""

    _domain_validator = validator(
        "matrix_domain",
        "matrix_server_fqdn",
        "weblite_domain",
        "web_domain",
        allow_reuse=True,
    )(validators.validate_domain)

    _cidr_validator = validator("cidr_block", allow_reuse=True)(
        validators.validate_16_cidr
    )

    _subnet_offset_validator = validator("subnet_offset", allow_reuse=True)(
        validators.validate_int_multiple_of_10
    )

    _identifiers_validator = validator(
        "rds_admin_user", "rds_db_name", allow_reuse=True
    )(validators.validate_identifier)

    base_config: AWSBaseConfig = Field(description="The base deployment configuration")

    cidr_block: str = Field(
        "10.55.0.0/16", description="The cidr to be used by the VPC. Must be a /16"
    )
    subnet_offset: int = Field(
        10, description="An offset applied to subnets in the VPC"
    )
    matrix_domain: str = Field(..., description="the domain")
    matrix_server_fqdn: Optional[str] = Field(
        description="The domain name the matrix server will be directly addressable at. Default: matrix.<matrix_domain>",
        advanced=True,
    )
    web_domain: Optional[str] = Field(
        description="The domain that the web-client Element will be available at. Default: web.<matrix_domain>",
        advanced=True,
    )
    weblite_domain: Optional[str] = Field(
        description="The domain that the client weblite will be available at. Default: lite.<matrix_domain>",
        advanced=True,
    )
    identity_server_enabled: bool = Field(
        True,
        description="Whether or not to enable the identity server. When disabled, the default matrix.org identity server will be used.",
    )
    rds_admin_password: str = Field(
        description="The root/admin password configured in the postgresql database. If not provided one is generated.",
        sensitive=True,
        is_generated=True,
        min_length=20,
        default_factory=lambda: util.generate_password(),
        advanced=True,
    )

    rds_admin_user: str = Field(
        "admin",
        description="The root/admin user configured in the postgresql database",
        advanced=True,
    )
    rds_allocated_storage_gb: int = Field(
        50,
        description="The initial storage allocated to the postgresql database",
    )
    rds_db_name: str = Field(
        "keanu",
        description="The name of the root/admin database in postgresql",
        advanced=True,
    )
    rds_instance_class: RdsInstanceTypes = Field(
        RdsInstanceTypes.db_t3_large,
        description="The size of the postgresql RDS database",
    )
    matrix_disk_allocation_gb: int = Field(
        35,
        description="The amount of disk space in gigabytes the operating system will have",
    )
    matrix_ebs_disk_allocation_gb: int = Field(
        100,
        description="The amount of disk space in gigabytes the matrix data mount will have",
    )
    matrix_instance_type: Ec2InstanceTypes = Field(
        Ec2InstanceTypes.t3_medium, description="The size of the matrix server"
    )
    sygnal_enabled: bool = Field(
        False,
        description="Whether or not sygnal will be installed. Set to true to enable push notifications",
    )
    ma1sd_enabled: bool = Field(
        True,
        description="Whether or not ma1sd will be installed. If disabled, users will not be able to associate email addresse swith their account or manually reset their passwords",
    )
    email_from_address: str = Field(
        ...,
        description="The email address that automated emails will come from. Default: noreply@<matrix_domain>",
    )
    friendly_name: str = Field(
        ...,
        description="The friendly name of the deployment. This is user facing (in emails, titles, etc)",
    )

    id: str = Field(..., description="A unique alpha-numeric id for this deployment")

    synapse_config: SynapseConfig = Field(..., description="The synapse config")
    ma1sd_config: Ma1sdConfig = Field(
        ..., description="The ma1sd identity server config"
    )
