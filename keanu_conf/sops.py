import enum
import os
import pathlib
import subprocess
from typing import Any, Dict, List, Optional, Tuple, Union


class SopsFileType(str, enum.Enum):
    json = "json"
    yaml = "yaml"
    binary = "binary"


def _sops(
    stdin: str,
    flags: List[str],
    cwd: Optional[pathlib.Path],
    environment: Dict[str, str],
) -> Tuple[Union[int, Any], str, Optional[bytes]]:
    env = os.environ.copy() | environment
    sopscmd = subprocess.Popen(
        ["sops"] + flags,
        cwd=cwd,
        env=env,
        stdin=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        stdout=subprocess.PIPE,
    )
    out, err = sopscmd.communicate(input=stdin.encode("utf-8"))
    out_str = out.decode("utf-8")
    return sopscmd.returncode, out_str, err


def encrypt(
    payload: str,
    input_type: SopsFileType,
    output_type: SopsFileType,
    cwd: Optional[pathlib.Path] = None,
    environment: Dict[str, str] = {},
) -> str:
    """Encrypt a payload of the given type using sops. Returns the encrypted cipher text as a string"""

    rc, out, err = _sops(
        payload,
        [
            "--encrypt",
            "--input-type",
            input_type.value,
            "--output-type",
            output_type.value,
            "/dev/stdin",
        ],
        cwd,
        environment,
    )
    if rc != 0:
        raise ChildProcessError(out)
    return out


def encrypt_to(
    payload: str,
    input_type: SopsFileType,
    output_type: SopsFileType,
    destination: pathlib.Path,
    cwd: Optional[pathlib.Path] = None,
    environment: Dict[str, str] = {},
):
    """Encrypt a payload of the given type using sops and saves it to destination."""

    rc, out, err = _sops(
        payload,
        [
            "--encrypt",
            "--input-type",
            input_type.value,
            "--output-type",
            output_type.value,
            "--output",
            str(destination),
            "/dev/stdin",
        ],
        cwd,
        environment,
    )

    if rc != 0:
        raise ChildProcessError(out)
    return out


def decrypt(
    ciphertext: str,
    input_type: SopsFileType,
    output_type: SopsFileType,
    cwd: Optional[pathlib.Path] = None,
    environment: Dict[str, str] = {},
) -> str:
    """Decrypt a payload of the given type using sops. Returns the plaintext as a string"""
    rc, out, err = _sops(
        ciphertext,
        [
            "--decrypt",
            "--input-type",
            input_type.value,
            "--output-type",
            output_type.value,
            "/dev/stdin",
        ],
        cwd,
        environment,
    )
    if rc != 0:
        raise ChildProcessError(out)
    return out


def decrypt_to(
    ciphertext: str,
    input_type: SopsFileType,
    output_type: SopsFileType,
    destination: pathlib.Path,
    cwd: Optional[pathlib.Path] = None,
    environment: Dict[str, str] = {},
) -> str:
    """Decrypt a payload of the given type, saving the plaintext to destination"""

    rc, out, err = _sops(
        ciphertext,
        [
            "--decrypt",
            "--input-type",
            input_type.value,
            "--output-type",
            output_type.value,
            "--output",
            str(destination),
            "/dev/stdin",
        ],
        cwd,
        environment,
    )
    if rc != 0:
        raise ChildProcessError(out)
    return out
