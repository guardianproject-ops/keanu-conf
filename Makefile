POETRY ?= poetry run
SRC := keanu_conf
TESTS := tests
fmt:
	$(POETRY) black $(SRC)
	$(POETRY) isort --profile black $(SRC)
lint:
	$(POETRY) flake8 $(SRC)
types:
	$(POETRY) mypy $(SRC)

test:
	$(POETRY) pytest $(TESTS)