
## Dev Setup
```
poetry install
poetry run keanu-conf --help
```

AWS region data comes from
https://raw.githubusercontent.com/jsonmaur/aws-regions/master/regions.json


ISO 639-1 language codes come from
https://datahub.io/core/language-codes/r/language-codes.json