from keanu_conf import interactive, models


def test_prompt():
    f = models.SynapseConfig.__fields__["encryption_enabled_by_default_for_room_type"]
    assert interactive.is_enum(f)


def test_model():
    f = models.KeanuConfig.__fields__["base_config"]
    assert interactive.is_singleton_model(f)

    f = models.Ma1sdConfig.__fields__["ma1sd_policies"]
    assert not interactive.is_singleton_model(f)
