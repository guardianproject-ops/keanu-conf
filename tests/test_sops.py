import json
import os
from tempfile import NamedTemporaryFile

from keanu_conf import sops

age_recipient = "age13u8jwjltnqukl9udl2vsj0l89ydq2qf32sfzfncghgf7lc36c50qnfn9ua"
age_key_file = os.path.join(os.path.dirname(__file__), "fixtures", "age.key")


def test_encrypt():
    plaintext = json.dumps({"foo": "bar"})
    ciphertext = sops.encrypt(
        plaintext,
        sops.SopsFileType.json,
        sops.SopsFileType.json,
        environment={"SOPS_AGE_RECIPIENTS": age_recipient},
    )
    j = json.loads(ciphertext)
    assert j["foo"] != "bar"
    assert j["foo"].startswith("ENC")


def test_encrypt_to():
    with NamedTemporaryFile() as fp:
        plaintext = json.dumps({"foo": "bar"})
        sops.encrypt_to(
            plaintext,
            sops.SopsFileType.json,
            sops.SopsFileType.json,
            destination=fp.name,
            environment={"SOPS_AGE_RECIPIENTS": age_recipient},
        )
        fp.seek(0)
        j = json.load(fp)
        assert j["foo"] != "bar"
        assert j["foo"].startswith("ENC")


def test_decrypt():
    print(age_key_file)
    with open(
        os.path.join(os.path.dirname(__file__), "fixtures", "example.sops.json"), "r"
    ) as f:
        ciphertext = f.read()
        plaintext = sops.decrypt(
            ciphertext,
            sops.SopsFileType.json,
            sops.SopsFileType.json,
            environment={"SOPS_AGE_KEY_FILE": age_key_file},
        )
        j = json.loads(plaintext)
        assert j["foo"] == "bar"


def test_decrypt_to():
    with open(
        os.path.join(os.path.dirname(__file__), "fixtures", "example.sops.json"), "r"
    ) as f:
        ciphertext = f.read()
    with NamedTemporaryFile() as fp:
        sops.decrypt_to(
            ciphertext,
            sops.SopsFileType.json,
            sops.SopsFileType.json,
            destination=fp.name,
            environment={"SOPS_AGE_KEY_FILE": age_key_file},
        )
        fp.seek(0)
        j = json.load(fp)
        assert j["foo"] == "bar"
