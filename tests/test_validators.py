import pytest

from keanu_conf import util, validators


def test_validate_signing_key():

    with pytest.raises(ValueError, match=r".*invalid.*"):
        validators.validate_signing_key("")

    key = util.generate_synapse_signing_key()

    assert validators.validate_signing_key(key) == key


def test_valid_identifier():
    assert "a-b-c" == validators.validate_identifier("a-b-c")
    assert "abc" == validators.validate_identifier("abc")
    assert "abc1" == validators.validate_identifier("abc1")

    with pytest.raises(ValueError, match=r".*contain.*"):
        validators.validate_identifier("ab_c")
    with pytest.raises(ValueError, match=r".*contain.*"):
        validators.validate_identifier("0ab_c")

    with pytest.raises(ValueError, match=r".*contain.*"):
        validators.validate_identifier("abc_")

    with pytest.raises(ValueError, match=r".*contain.*"):
        validators.validate_identifier("Abc")


def test_valid_domain():
    assert "foobar.com" == validators.validate_domain("foobar.com")
    with pytest.raises(ValueError, match=r".*invalid.*"):
        validators.validate_domain("foobar")

    with pytest.raises(ValueError, match=r".*invalid.*"):
        validators.validate_domain(".foobar.com")


def test_16_cidr():
    assert "192.168.0.1/16" == validators.validate_16_cidr("192.168.0.1/16")
    assert "10.50.0.0/16" == validators.validate_16_cidr("10.50.0.0/16")
    with pytest.raises(ValueError, match=r".*invalid.*"):
        validators.validate_16_cidr("10.50.0.0")
    with pytest.raises(ValueError, match=r".*invalid.*"):
        validators.validate_16_cidr("10.50.0.0/32")
    with pytest.raises(ValueError, match=r".*invalid.*"):
        validators.validate_16_cidr("10.50.0.0/8")

    with pytest.raises(ValueError, match=r".*invalid.*"):
        validators.validate_16_cidr("")

    with pytest.raises(ValueError, match=r".*invalid.*"):
        validators.validate_16_cidr("foobar")

    with pytest.raises(ValueError, match=r".*invalid.*"):
        validators.validate_16_cidr("10.50")
    with pytest.raises(ValueError, match=r".*invalid.*"):
        validators.validate_16_cidr("10.500.0.0/16")


def test_valid_int_multiple_of_10():
    assert 10 == validators.validate_int_multiple_of_10(10)
    assert 20 == validators.validate_int_multiple_of_10(20)
    assert 30 == validators.validate_int_multiple_of_10(30)
    assert 100 == validators.validate_int_multiple_of_10(100)
    with pytest.raises(ValueError, match=r".*multiple.*"):
        validators.validate_int_multiple_of_10(5)
