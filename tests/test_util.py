import re
import string

import pytest

from keanu_conf import util


@pytest.mark.parametrize("execution_number", range(20))
def test_generate_password(execution_number):
    v = util.generate_password()
    assert len(v) == 30
    assert re.match("^[a-zA-Z0-9]+$", v) is not None

    alphabet = string.ascii_letters + string.punctuation
    v = util.generate_password(50, alphabet)
    assert len(v) == 50
    for c in v:
        assert c in alphabet


@pytest.mark.parametrize("execution_number", range(20))
def test_random_identifier(execution_number):
    v = util.random_identifier(30)
    assert len(v) == 30
    for c in v:
        assert c in string.ascii_letters
